﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task5
{
    //NEW FEATURE C#9.0  // Records // [1]
    public record ArithmeticExpression
    {
        //NEW FEATURE C#9.0  // Init only setters // [2]
        public double Operand1 { get; init; }
        public double Operand2 { get; init; }

        public ArithmeticOperationTypes Operation { get; init; }
        public char Operator { get; init; }

        public override string ToString()
        {
            return $"{Operand1} {Operator} {Operand2}";
        }
    }

    public enum ArithmeticOperationTypes
    {
        None,
        Addition,
        Subtraction,
        Multiplication,
        Division
    }
}
