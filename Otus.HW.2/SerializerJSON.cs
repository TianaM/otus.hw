﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otus.HW.Task2
{
    class SerializerJSON
    {
        /// <summary> Serialize from object to JSON </summary>
        /// <param name="obj">any object</param>
        /// <returns>JSON</returns>
        public static string SerializeFromObjectToJSON(object obj)
        {
            string str = JsonConvert.SerializeObject(obj);

            return str;
        }

        /// <summary> Deserialize from JSON to object</summary>
        /// <param name="json">string in JSON format</param>
        /// <returns>object</returns>
        public static T DeserializeFromJSONToObject<T>(string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}
