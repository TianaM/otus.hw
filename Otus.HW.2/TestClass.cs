﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task2
{
    public class F
    {
        public int prop1 { get { return j1 + j2; } set { return; } }
        public int prop2 { get; set; }
        public int i1, i2, i3, i4, i5;
        private int j1, j2;
        
        public static F Get()
        {
            return new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, j1 = 10, j2 = 20, prop2 = 35 };
        }
    }
}
