﻿using System;
using System.Linq;
using System.Reflection;

namespace Otus.HW.Task2
{
    /// <summary> Serializer </summary>
    public static class SerializerCSV
    {
        /// <summary> Serialize from object to CSV </summary>
        /// <param name="obj">any object</param>
        /// <returns>CSV</returns>
        public static string SerializeFromObjectToCSV(object obj)
        {
            Type type = obj.GetType();
            string strResult = "TypeName:" + type.ToString() + ",";
            PropertyInfo[] props = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in props)
            {
                strResult += prop.Name + ":" + prop.GetValue(obj: obj).ToString() + ",";
            }

            FieldInfo[] fieldInfos = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (FieldInfo fieldInfo in fieldInfos)
            {
                strResult += fieldInfo.Name + ":" + fieldInfo.GetValue(obj: obj).ToString() + ",";
            }

            return strResult.TrimEnd(',');
        }

        /// <summary> Deserialize from CSV to object</summary>
        /// <param name="csv">string in CSV format</param>
        /// <returns>object</returns>
        public static object DeserializeFromCSVToObject(string csv)
        {
            var dataCSV = csv.Split(',');
            if (dataCSV.Length == 0) return null;

            var typeName = dataCSV.First().Split(':').Last();
            Type objType = Type.GetType(typeName, false);
            if (objType == null) return null;

            object obj = Activator.CreateInstance(type: objType);

            foreach (var data in dataCSV)
            {
                var pair = data.Split(':');

                if (pair[0] == "TypeName") continue;
                else
                {
                    var fName = pair[0];
                    var fValue = pair[1];

                    FieldInfo field = objType.GetField(fName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (field != null) field.SetValue(obj: obj, value: Convert.ChangeType(fValue, field.FieldType));
                    else
                    {
                        PropertyInfo property = objType.GetProperty(fName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                        if (property != null) property.SetValue(obj: obj, value: Convert.ChangeType(fValue, property.PropertyType));
                    }
                }
            }

            return obj;
        }
    }
}
