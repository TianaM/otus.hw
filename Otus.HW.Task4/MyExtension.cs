﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task4
{
    public static class MyExtension //Extensions
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T maxElem = default;

            foreach (var curElem in e)
            {
                var maxValue = getParameter(maxElem);
                var value = getParameter(curElem);

                if (value > maxValue)
                {
                    maxValue = value;
                    maxElem = curElem;
                }
            }
            return maxElem;

        }
    }
}
