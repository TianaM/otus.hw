﻿using System;
using System.Drawing;

namespace Otus.HW.Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Class [TextMsg]");
            Console.WriteLine("-----------------------------------------------------------------------");
            var Msg1 = new TextMsg("Message1");
            var Msg2 = new TextMsg("Message2");
            var copyMsg1 = (TextMsg)Msg1.Clone();
            var cloneMsg2 = Msg2.MyCopy();
            Console.WriteLine($"[TextMsg] Msg1: {Msg1}");
            Console.WriteLine($"[TextMsg] Msg2: {Msg2}");
            Console.WriteLine($"[TextMsg] copyMsg1: {copyMsg1}");
            Console.WriteLine($"[TextMsg] cloneMsg2: {cloneMsg2}");
            Console.WriteLine();
            Console.WriteLine("Changes: Msg1.Msg = \"NewMessage1\" and Msg2.Msg = \"NewMessage2\"");
            Msg1.Msg = "NewMessage1";
            Msg2.Msg = "NewMessage2";
            Console.WriteLine($"[TextMsg] Msg1: {Msg1}");
            Console.WriteLine($"[TextMsg] Msg2: {Msg2}");
            Console.WriteLine($"[TextMsg] copyMsg1: {copyMsg1}");
            Console.WriteLine($"[TextMsg] cloneMsg2: {cloneMsg2}");

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Class [ColoredTextMsg]");
            Console.WriteLine("-----------------------------------------------------------------------");
            var cMsg1 = new ColoredTextMsg("Message1","red");
            var cMsg2 = new ColoredTextMsg("Message2","green");
            var copyCMsg1 = (ColoredTextMsg)cMsg1.Clone();
            var cloneCMsg2 = cMsg2.MyCopy();
            Console.WriteLine($"[ColoredTextMsg] cMsg1: {cMsg1}");
            Console.WriteLine($"[ColoredTextMsg] cMsg2: {cMsg2}");
            Console.WriteLine($"[ColoredTextMsg] copyCMsg1: {copyCMsg1}");
            Console.WriteLine($"[ColoredTextMsg] cloneCMsg2: {cloneCMsg2}");
            Console.WriteLine();
            Console.WriteLine("Changes: cMsg1.Msg = \"NewMessage1\" and cMsg2.Msg = \"NewMessage2\"");
            cMsg1.Msg = "NewMessage1";
            cMsg2.Msg = "NewMessage2";
            Console.WriteLine($"[ColoredTextMsg] cMsg1: {cMsg1}");
            Console.WriteLine($"[ColoredTextMsg] cMsg2: {cMsg2}");
            Console.WriteLine($"[ColoredTextMsg] copyCMsg1: {copyCMsg1}");
            Console.WriteLine($"[ColoredTextMsg] cloneCMsg2: {cloneCMsg2}");

            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Class [FormattedColoredTextMsg]");
            Console.WriteLine("-----------------------------------------------------------------------");
            var fMsg1 = new FormattedColoredTextMsg("Message1","red", new FontStyle( "Arial", "Bold" ,12));
            var fMsg2 = new FormattedColoredTextMsg("Message2", "green", new FontStyle("ArialNarrow", "Italic",10));
            var copyFMsg1 = (ColoredTextMsg)fMsg1.Clone();
            var cloneFMsg2 = fMsg2.MyCopy();
            Console.WriteLine($"[FormatedColoredTextMsg] fMsg1: {fMsg1}");
            Console.WriteLine($"[FormatedColoredTextMsg] fMsg2: {fMsg2}");
            Console.WriteLine($"[FormatedColoredTextMsg] copyFMsg1: {copyFMsg1}");
            Console.WriteLine($"[FormatedColoredTextMsg] cloneFMsg2: {cloneFMsg2}");
            Console.WriteLine();
            Console.WriteLine("Changes: fMsg1.Msg = \"NewMessage1\" and fMsg2.Msg = \"NewMessage2\"");
            fMsg1.FontStyle.fSize = 8;
            fMsg1.FontStyle.fSize = 18;
            Console.WriteLine($"[FormatedColoredTextMsg] fMsg1: {fMsg1}");
            Console.WriteLine($"[FormatedColoredTextMsg] fMsg2: {fMsg2}");
            Console.WriteLine($"[FormatedColoredTextMsg] copyFMsg1: {copyFMsg1}");
            Console.WriteLine($"[FormatedColoredTextMsg] cloneFMsg2: {cloneFMsg2}");



            Console.ReadLine();
        }
    }

}
