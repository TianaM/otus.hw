﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task4
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; private set; }

        public FileArgs(string _fileName)
        {
            FileName = _fileName;
        }

    }

}
