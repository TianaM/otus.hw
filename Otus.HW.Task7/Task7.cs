﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Runtime;

namespace Otus.HW.Task7
{
    public static class Task7
    {
        private static int[] Array1 = new int[100000];
        private static int[] Array2 = new int[1000000];
        private static int[] Array3 = new int[10000000];

        static Stopwatch StopW = new Stopwatch();
        static object Locker = new object();
        static int ThreadsCount = 8;

        public static void Start()
        {
            int[] mas = new int[] { 1, 2, 3, 4, 5 };
            mas.AsParallel().Sum();
            mas.AsParallel().Sum();

            RandomInitializeArray(Array1);
            RandomInitializeArray(Array2);
            RandomInitializeArray(Array3);

            SimpleCalcSum(Array1);
            SimpleCalcSum(Array1);
            SimpleCalcSum(Array1);
            ThreadsCalcSum(Array1, ThreadsCount);
            ThreadsCalcSum(Array1, ThreadsCount);
            ThreadsCalcSum(Array1, ThreadsCount);
            PLINQCalcSum(Array1);
            PLINQCalcSum(Array1);
            PLINQCalcSum(Array1);

            Console.WriteLine();

            SimpleCalcSum(Array2);
            SimpleCalcSum(Array2);
            SimpleCalcSum(Array2);
            ThreadsCalcSum(Array2, ThreadsCount);
            ThreadsCalcSum(Array2, ThreadsCount);
            ThreadsCalcSum(Array2, ThreadsCount);
            PLINQCalcSum(Array2);
            PLINQCalcSum(Array2);
            PLINQCalcSum(Array2);

            Console.WriteLine();

            SimpleCalcSum(Array3);
            SimpleCalcSum(Array3);
            SimpleCalcSum(Array3);
            ThreadsCalcSum(Array3, ThreadsCount);
            ThreadsCalcSum(Array3, ThreadsCount);
            ThreadsCalcSum(Array3, ThreadsCount);
            PLINQCalcSum(Array3);
            PLINQCalcSum(Array3);
            PLINQCalcSum(Array3);
        }

        private static void RandomInitializeArray(int[] _array, int _maxValue = 100)
        {
            Random Rand = new Random();
            for (int i = 0; i < _array.Length; i++)
            {
                _array[i] = Rand.Next(_maxValue);
            }
        }

        private static long SimpleCalcSum(int[] _array)
        {
            long Sum = 0;

            StopW.Restart();
            for (int i = 0; i < _array.Length; i++)
            {
                Sum += _array[i];
            }
            StopW.Stop();
            Console.WriteLine("{SimpleCalcSum}   Array[" + _array.Length + "]  Sum = " + Sum + "  Total time is " + StopW.ElapsedMilliseconds);

            return Sum;
        }

        private static long ThreadsCalcSum(int[] _array, int _threadsCount)
        {
            long Sum = 0;

            StopW.Restart();

            int block = _array.Length / _threadsCount;
            int tail = (_array.Length % _threadsCount == 0) ? 0 : 1;

            List<Thread> TList = new List<Thread>();
            for (int i = 0; i < _threadsCount - tail; i++)
            {
                int ST = i * block;
                int END = (i + 1) * block;

                Thread t = new Thread(() => TCalcSum(_array, ST, END, ref Sum));
                t.Start();
                TList.Add(t);

            }

            if (tail != 0)
            {
                Thread t = new Thread(() => TCalcSum(_array, (_threadsCount - 1) * block, _array.Length, ref Sum));
                t.Start();
                TList.Add(t);
            }
            
            foreach (var t in TList) t.Join();

            StopW.Stop();
            Console.WriteLine("{ThreadsCalcSum: " + ThreadsCount.ToString() + " threads}  Array[" + _array.Length + "]  Sum = " + Sum + "  Total time is " + StopW.ElapsedMilliseconds);

            return Sum;
        }
        
        private static void TCalcSum(int[] mas, int s, int e, ref long Summa)
        {
            lock (Locker)
            {
                long Sum = 0;
                for (int J = s; J < e; J++)
                {
                    Sum += mas[J];
                }
                
                Summa += Sum;
            }
        }

        private static long PLINQCalcSum(int[] _array)
        {
            long Sum = 0;

            StopW.Restart();

            Sum = _array.AsParallel().Sum();

            StopW.Stop();
            Console.WriteLine("{PLINQCalcSum}    Array[" + _array.Length + "]  Sum = " + Sum + "  Total time is " + StopW.ElapsedMilliseconds);

            return Sum;
        }

    }

}
