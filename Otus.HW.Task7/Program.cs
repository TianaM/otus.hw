﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Runtime;

namespace Otus.HW.Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            Task7.Start();

            Console.Read();
        }
    }

}
