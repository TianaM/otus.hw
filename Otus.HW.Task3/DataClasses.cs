﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task3
{    
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        /// <value></value>
        public long Id { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        /// <value></value>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        /// <value></value>
        public string Password { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        /// <value></value>
        public string LastName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        /// <value></value>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        /// <value></value>
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        /// <value></value>
        public string Email { get; set; }

        /// <summary>
        /// Дата создания аккаунта
        /// </summary>
        /// <value></value>
        public DateTime CreatedDate { get; set; }

        //public User(string login,)
        //{

        //}

        public override string ToString()
        {
            return String.Format($" User {Login} ({FirstName} {MiddleName} {LastName}) e-mail: {Email} [UserId: {Id}]");
            /// return $"login={Login}";
        }
    }

    /// <summary>
    /// Курс
    /// </summary>
    public class Course
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        /// <value></value>
        public long Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        /// <value></value>
        public double Cost { get; set; }

        /// <summary>
        /// Длительность обучения в месяцах
        /// </summary>
        /// <value></value>
        public double Duration { get; set; }

        public override string ToString()
        {
            return String.Format($"Course \"{Name}\"  Duration: {Duration} months  Cost: {Cost} $ [CourseId: {Id}]");
        }
    }

    /// <summary>
    /// Оплата
    /// </summary>
    public class PaymentInfo
    {
        /// <summary>
        /// Идентификатор платежа
        /// </summary>
        /// <value></value>
        public long Id { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        /// <value></value>
        public long UserId { get; set; }

        ///// <summary>
        ///// Пользователь
        ///// </summary>
        ///// <value></value>
        //public User User { get; set; }

        /// <summary>
        /// Id курса
        /// </summary>
        /// <value></value>
        public long CourseId { get; set; }

        ///// <summary>
        ///// Курс
        ///// </summary>
        ///// <value></value>
        //public Course Course { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        /// <value></value>
        public double PaymentSum { get; set; }

        /// <summary>
        /// Дата платежа
        /// </summary>
        /// <value></value>
        public DateTime PaymentDate { get; set; }

        public override string ToString()
        {
            return String.Format($"Payment at {PaymentDate} {PaymentSum}$  [UserId: {UserId}] [CourseId: {CourseId}]");
            /// return $"login={Login}";
        }
    }
}
