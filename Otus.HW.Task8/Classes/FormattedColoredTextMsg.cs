﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task8
{
    public class FormattedColoredTextMsg : ColoredTextMsg, ICloneable
    {
        public FontStyle FontStyle { get; set; }

        public FormattedColoredTextMsg()
        { }
        public FormattedColoredTextMsg(string msg, string color, FontStyle fstyle) : base(msg, color)
        {
            FontStyle = fstyle;
        }
        public FormattedColoredTextMsg(FormattedColoredTextMsg prototype) : base(prototype)
        {
            FontStyle = new FontStyle(prototype.FontStyle);
        }

        public override FormattedColoredTextMsg MyCopy()
        {
            return new FormattedColoredTextMsg(this);
        }

        public override string ToString()
        {
            return $"\"{Msg}\"  Color: {Color}  {{{FontStyle}}}";
        }
    }
}
