﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Diagnostics;

namespace Otus.HW.Task2
{
    class Program
    {
        //static public Stopwatch StW = new Stopwatch();

        static void Main(string[] args)
        {
            MyReflectionSpeedTest();
            //Console.ReadLine();

            Console.WriteLine("\n");

            NewtonsoftJSONSpeedTest();
            Console.ReadLine();

        }

        static void MyReflectionSpeedTest()
        {
            Stopwatch StW = new Stopwatch();

            F myF = F.Get();
            string serializedObject = SerializerCSV.SerializeFromObjectToCSV(myF);
            Console.WriteLine("Serialized object in string:");
            Console.WriteLine(serializedObject);
            F deserializedF = (F)SerializerCSV.DeserializeFromCSVToObject(serializedObject);

            #region SpeedTest
            StW.Start();
            for (int i = 0; i < 100000; i++)
            { serializedObject = SerializerCSV.SerializeFromObjectToCSV(myF); }

            StW.Stop();
            Console.WriteLine($"Serialization time: {StW.ElapsedMilliseconds} milliseconds");

            // Deserialization
            StW.Restart();
            for (int i = 0; i < 100000; i++)
            { deserializedF = (F)SerializerCSV.DeserializeFromCSVToObject(serializedObject); }
            StW.Stop();
            Console.WriteLine($"Deserialization time: {StW.ElapsedMilliseconds} milliseconds");
            #endregion
        }

        static void NewtonsoftJSONSpeedTest()
        {
            Stopwatch StW = new Stopwatch();

            F myF = F.Get();
            string serializedObject = SerializerJSON.SerializeFromObjectToJSON(myF);
            Console.WriteLine("Serialized object in JSON string:");
            Console.WriteLine(serializedObject);
            F deserializedF = SerializerJSON.DeserializeFromJSONToObject<F>(serializedObject);

            #region SpeedTest
            // Serialization
            StW.Start();
            for (int i = 0; i < 100000; i++)
            { serializedObject = SerializerJSON.SerializeFromObjectToJSON(myF); }

            StW.Stop();
            Console.WriteLine($"Serialization time: {StW.ElapsedMilliseconds} milliseconds");

            // Deserialization
            StW.Restart();
            for (int i = 0; i < 100000; i++)
            { deserializedF = SerializerJSON.DeserializeFromJSONToObject<F>(serializedObject); }
            StW.Stop(); 
            Console.WriteLine($"Deserialization time: {StW.ElapsedMilliseconds} milliseconds");
            #endregion
        }
    }

}
