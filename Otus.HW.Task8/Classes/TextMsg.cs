﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task8
{
    /// <summary>
    /// Base class
    /// </summary>
    public class TextMsg : IMyCloneable<TextMsg>, ICloneable
    {
        public string Msg { get; set; }

        public TextMsg()
        { }
        public TextMsg(string msg)
        {
            Msg = msg;
        }
        public TextMsg(TextMsg prototype)
        {
            Msg = prototype.Msg;
        }

        public virtual TextMsg MyCopy()
        {
            return new TextMsg(this);
        }

        public object Clone()
        {
            return MyCopy();
        }

        public override string ToString()
        {
            return $"\"{Msg}\"";
        }
    }
}
