﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Otus.HW.Task4
{
    class FileDirectoryViewer//FileSearcher
    {
        private string targetDirectory;
        //private string fileNameToSearch;
        
        public bool SearchFinished = false;

        public event EventHandler<FileArgs> FileFound;

        public void Start(string _targetDirectory, CancellationToken _token)//, string _fileName = null)
        {
            targetDirectory = _targetDirectory;
            //fileNameToSearch = _fileName;

            var dirInfo = new DirectoryInfo(targetDirectory);
            if (!dirInfo.Exists)
            {
                return;
            }

            var dir = Directory.GetFiles(targetDirectory)
                .Select(p => Path.GetFileName(p))
                .ToList();

            foreach (var fName in dir)
            {
                if (!_token.IsCancellationRequested)
                {
                    FileFound?.Invoke(this, new FileArgs(fName));
                    Thread.Sleep(100);
                }
            }

            SearchFinished = true;
        }


    }
}
