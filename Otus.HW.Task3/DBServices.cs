﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Otus.HW.Task3
{
    public static class DBServices
    {
        const string DBName = "otustaskdb";
        const string connectionStringSA = "Host=localhost;Username=OTUS;Password=Otus123;Database=postgres";
        static string connectionString = $"Host=localhost;Username=OTUS;Password=Otus123;Database={DBName}";

        /// <summary>
        /// Создание базы данных otustaskdb
        /// </summary>
        public static void CreateDB()
        {
            using (var connection = new NpgsqlConnection(connectionStringSA))
            {
                connection.Open();

                //using (var transaction = connection.BeginTransaction())
                //{
                //try
                //{
                var sql = @"SELECT COUNT (*) FROM pg_database WHERE datname = 'otustaskdb2'";
                using (var cmd1 = new NpgsqlCommand(sql, connection))
                {
                    var res = (long)cmd1.ExecuteScalar();

                    if (res == 0)
                    {
                        sql = $@"CREATE DATABASE {DBName}";
                        using (var cmd2 = new NpgsqlCommand(sql, connection))
                        {
                            cmd2.ExecuteNonQuery();
                        }

                    }
                }

                //transaction.Commit();
                //}
                //catch (Exception e)
                //{
                //    //transaction.Rollback();
                //    //Console.WriteLine($"Rolled back the transaction");
                //    return;
                //}
                //}
            }
        }

        /// <summary> Создание таблицы USERS в базе </summary>
        public static void CreateUsersTable()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            CREATE SEQUENCE IF NOT EXISTS users_id_seq;

                            CREATE TABLE IF NOT EXISTS users
                            (
                                id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('users_id_seq'),
                                login           CHARACTER VARYING(255)      NOT NULL,
                                password        CHARACTER VARYING(255)      NOT NULL,
                                first_name      CHARACTER VARYING(255)      NOT NULL,
                                last_name       CHARACTER VARYING(255)      NOT NULL,
                                middle_name     CHARACTER VARYING(255),
                                email           CHARACTER VARYING(255)      NOT NULL,
                                created_date    TIMESTAMP WITHOUT TIME ZONE NOT NULL,

                                CONSTRAINT users_pkey PRIMARY KEY (id),
                                CONSTRAINT users_email_unique UNIQUE (email)
                            );
                        ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    var affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                }
            }
        }

        /// <summary> Создание таблицы COURSES в базе </summary>
        public static void CreateCoursesTable()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            CREATE SEQUENCE IF NOT EXISTS courses_id_seq;

                            CREATE TABLE IF NOT EXISTS courses
                            (
                                id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('courses_id_seq'),
                                name            CHARACTER VARYING(255)      NOT NULL,
                                cost            NUMERIC                     NOT NULL,
                                duration        NUMERIC                     NOT NULL,

                                CONSTRAINT courses_pkey PRIMARY KEY (id)
                            );
                        ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    var affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                }
            }
        }

        /// <summary> Создание таблицы PAYMENTS в базе </summary>
        public static void CreatePaymentsTable()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            CREATE SEQUENCE IF NOT EXISTS payments_id_seq;

                            CREATE TABLE IF NOT EXISTS payments
                            (
                                id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('payments_id_seq'),
                                user_id         BIGINT                      NOT NULL,
                                course_id       BIGINT                      NOT NULL,
                                payment_sum     NUMERIC                     NOT NULL,
                                payment_date    TIMESTAMP WITHOUT TIME ZONE NOT NULL,

                                CONSTRAINT payments_pkey PRIMARY KEY (id),
                                CONSTRAINT payments_fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
                                CONSTRAINT payments_fk_course_id FOREIGN KEY (course_id) REFERENCES courses(id) ON DELETE CASCADE
                            );
                        ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    var affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                }
            }
        }


        // INSERT //
        /// <summary> Добавление нового пользователя в БД </summary>
        public static void InsertUserInDB(User _User)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            INSERT INTO users(login, password, first_name, last_name, middle_name, email, created_date) 
                            VALUES (:login, :password, :first_name, :last_name, :middle_name, :email, LOCALTIMESTAMP) 
                            RETURNING id;
                            ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    try
                    {
                        var parameters = cmd.Parameters;
                        parameters.Add(new NpgsqlParameter("login", _User.Login));
                        parameters.Add(new NpgsqlParameter("password", _User.Password));
                        parameters.Add(new NpgsqlParameter("first_name", _User.FirstName));
                        parameters.Add(new NpgsqlParameter("last_name", _User.LastName));
                        parameters.Add(new NpgsqlParameter("middle_name", _User.MiddleName));
                        parameters.Add(new NpgsqlParameter("email", _User.Email));

                        _User.Id = long.Parse(cmd.ExecuteScalar().ToString());

                        Console.WriteLine($"Insert into USERS table was successfull: {_User}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error occured during insert into USERS table: {_User}");
                    }
                }
            }
        }

        /// <summary> Добавление нового курса в БД </summary>
        public static void InsertCourseInDB(Course _Course)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            INSERT INTO courses(name, cost, duration) 
                            VALUES (:name, :cost, :duration) 
                            RETURNING id;
                            ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    try
                    {
                        var parameters = cmd.Parameters;
                        parameters.Add(new NpgsqlParameter("name", _Course.Name));
                        parameters.Add(new NpgsqlParameter("cost", _Course.Cost));
                        parameters.Add(new NpgsqlParameter("duration", _Course.Duration));

                        _Course.Id = long.Parse(cmd.ExecuteScalar().ToString());

                        Console.WriteLine($"Insert into COURSES table was successfull: {_Course}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error occured during insert into COURSES table: {_Course}");
                    }
                }
            }
        }

        /// <summary> Добавление нового платежа в БД </summary>
        public static void InsertPaymentInDB(PaymentInfo _Payment)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            INSERT INTO payments(user_id, course_id, payment_sum, payment_date)
	                        VALUES (:user_id, :course_id, :payment_sum, LOCALTIMESTAMP) 
                            RETURNING id;
                            ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    try
                    {
                        var parameters = cmd.Parameters;
                        parameters.Add(new NpgsqlParameter("user_id", _Payment.UserId));
                        parameters.Add(new NpgsqlParameter("course_id", _Payment.CourseId));
                        parameters.Add(new NpgsqlParameter("payment_sum", _Payment.PaymentSum));

                        _Payment.Id = long.Parse(cmd.ExecuteScalar().ToString());

                        Console.WriteLine($"Insert into PAYMENTS table was successfull: {_Payment}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error occured during insert into PAYMENTS table: {_Payment}");
                    }
                }
            }
        }


        // SELECT //
        /// <summary> Добавление нового пользователя в БД </summary>
        public static List<User> GetUsersFromDB()
        {
            List<User> Users = new List<User>();
            User Usr;

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            SELECT id, login, password, first_name, last_name, middle_name, email, created_date 
                            FROM public.users;
                            ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    try
                    {
                        NpgsqlDataReader Reader = cmd.ExecuteReader();

                        while (Reader.Read())
                        {
                            try
                            {
                                Usr = new User();
                                Usr.Id = Reader.GetInt64(0);
                                Usr.Login = Reader.GetString(1);
                                Usr.Password = Reader.GetString(2);
                                Usr.FirstName = Reader.GetString(3);
                                Usr.LastName = Reader.GetString(4);
                                Usr.MiddleName = Reader.GetString(5);
                                Usr.Email = Reader.GetString(6);
                                Usr.CreatedDate = (DateTime)Reader.GetTimeStamp(7);

                                Users.Add(Usr);
                            }
                            catch { }
                        }

                        return Users;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error occured during select operation from USERS table!");
                        return null;
                    }
                }
            }
        }

        public static List<Course> GetCoursesFromDB()
        {
            List<Course> Courses = new List<Course>();
            Course course;

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            SELECT id, name, cost, duration 
	                        FROM courses;
                            ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    try
                    {
                        NpgsqlDataReader Reader = cmd.ExecuteReader();

                        while (Reader.Read())
                        {
                            try
                            {
                                course = new Course();
                                course.Id = Reader.GetInt64(0);
                                course.Name = Reader.GetString(1);
                                course.Cost = Reader.GetDouble(2);
                                course.Duration = Reader.GetDouble(3);

                                Courses.Add(course);
                            }
                            catch { }
                        }

                        return Courses;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error occured during select operation from COURSES table!");
                        return null;
                    }
                }
            }
        }

        public static List<PaymentInfo> GetPaymentsFromDB()
        {
            List<PaymentInfo> Payments = new List<PaymentInfo>();
            PaymentInfo Pay;

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var sql = @"
                            SELECT id, user_id, course_id, payment_sum, payment_date
	                        FROM payments;
                            ";

                using (var cmd = new NpgsqlCommand(sql, connection))
                {
                    try
                    {
                        NpgsqlDataReader Reader = cmd.ExecuteReader();

                        while (Reader.Read())
                        {
                            try
                            {
                                Pay = new PaymentInfo();
                                Pay.Id = Reader.GetInt64(0);
                                Pay.UserId = Reader.GetInt64(1);
                                Pay.CourseId = Reader.GetInt64(2);
                                Pay.PaymentSum = Reader.GetDouble(3);
                                Pay.PaymentDate = (DateTime)Reader.GetTimeStamp(4);

                                Payments.Add(Pay);
                            }
                            catch { }
                        }

                        return Payments;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error occured during select operation from PAYMENTS table!");
                        return null;
                    }
                }
            }
        }
    }
}
