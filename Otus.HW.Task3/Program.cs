﻿using System;

namespace Otus.HW.Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            MainServices.StartWork();

            Console.ReadLine();
        }
    }
}
