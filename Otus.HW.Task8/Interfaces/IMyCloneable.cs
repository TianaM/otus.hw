﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task8
{
    /// <summary>
    /// My interface for implementing the prototype pattern 
    /// </summary>
    public interface IMyCloneable<out T>
    {
        public T MyCopy();
    }

}
