﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Otus.HW.Task5
{
    class Calculator
    {
        public ArithmeticExpression Expression;
        public double result;
        public char separator { get; init; } = double.TryParse("1.5", out double res) ? '.' : ',';
        public void Start()
        {
            Console.WriteLine("=========== SUPER MEGA CALCULATOR ===========\n");
            Console.WriteLine("Input expression format: operand1 + operand2");
            Console.WriteLine("Avialable operators: + - / *");
            Console.WriteLine("Press ENTER to get result. Press ESC to exit.\n");
            Console.WriteLine("=============================================\n");

            bool cancel = false;
            bool waitNextKey;
            string expression = "";
            do
            {
                expression = "";
                Console.WriteLine("\nEnter expression to calculate:");
                do
                {
                    if (ConsoleReading(ref expression, out waitNextKey))
                        return;
                    if (waitNextKey)
                        continue;
                } while (waitNextKey);

                if(ParseExpression(expression,out double @v1, out double @v2, out char @op, out ArithmeticOperationTypes @operation))
                {
                    //NEW FEATURE C#9.0 // Target-typed new expressions // [3]
                    Expression = new() { Operand1 = v1, Operand2 = v2, Operator = op, Operation = operation };

                    Calculate();

                    Console.WriteLine($"Calculation result: {Expression} = {result}");
                }
            }
            while (!cancel);
        }

        public bool ConsoleReading(ref string str, out bool waitNextKey)
        {
            while (!Console.KeyAvailable)
            { }

            var keyInfo = Console.ReadKey(true);
            waitNextKey = false;

            if (keyInfo.Key == ConsoleKey.Escape)
            {
                str = "";
                return true;
            }

            if (keyInfo.Key != ConsoleKey.Enter)
            {
                if (IsCorrectSymbol(keyInfo.KeyChar))
                {
                    str += keyInfo.KeyChar;

                    Console.Write(keyInfo.KeyChar);
                    waitNextKey = true;
                    return false;
                }
                else
                {
                    waitNextKey = true;
                    return false;
                }
            }

            Console.WriteLine();

            return false;
        }

        //NEW FEATURE C#9.0  // Pattern matching enhancements // [4]
        public bool IsCorrectSymbol(char c) => c is (>= '0' and <= '9') or '+' or '-' or '/' or '*' || c == separator;


        public bool ParseExpression(string inputedStr, out double v1, out double v2, out char op, out ArithmeticOperationTypes operation)
        {
            v1 = 0;
            v2 = 0;
            op = ' ';
            operation = ArithmeticOperationTypes.None;

            //NEW FEATURE in C#9 // Native-sized integers // [5]
            nint minus = 1;

            string expression = inputedStr.Trim();

            if (expression.IndexOf('-') == 0)
            {
                minus = -1;
                expression = expression.Remove(0, 1);
            }

            if (expression.ToList().Count(c => c == '+') == 1) { op = '+'; operation = ArithmeticOperationTypes.Addition; }
            if (expression.ToList().Count(c => c == '-') == 1) { op = '-'; operation = ArithmeticOperationTypes.Subtraction; }
            if (expression.ToList().Count(c => c == '/') == 1) { op = '/'; operation = ArithmeticOperationTypes.Division; }
            if (expression.ToList().Count(c => c == '*') == 1) { op = '*'; operation = ArithmeticOperationTypes.Multiplication; }

            string[] expr = expression.Split(op);
            if (op == ' ' || expr.Length != 2) {Console.WriteLine("Invalid expression!"); return false; }
            else
            {
                try
                {
                    v1 = minus * double.Parse(expr[0]);

                    try
                    {
                        v2 = double.Parse(expr[1]);
                    }
                    catch { Console.WriteLine("Invalid expression!"); return false; }
                }
                catch { Console.WriteLine("Invalid expression!"); return false; }
            }

            return true;
        }

        public void Calculate()
        {
            result = Expression.Operation switch
            {
                ArithmeticOperationTypes.Addition => Expression.Operand1 + Expression.Operand2,
                ArithmeticOperationTypes.Subtraction => Expression.Operand1 - Expression.Operand2,
                ArithmeticOperationTypes.Multiplication => Expression.Operand1 * Expression.Operand2,
                ArithmeticOperationTypes.Division => Expression.Operand1 / Expression.Operand2,
                _ => throw new FormatException("Invalid expression!")
            };
        }

    }
}
