﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task8
{
    public class ColoredTextMsg : TextMsg, ICloneable
    {
        public string Color { get; set; }

        public ColoredTextMsg()
        { }
        public ColoredTextMsg(string msg, string color) : base(msg)
        {
            Color = color;
        }
        public ColoredTextMsg(ColoredTextMsg prototype) : base(prototype)
        {
            Color = prototype.Color;
        }

        public override ColoredTextMsg MyCopy()
        {
            return new ColoredTextMsg(this);
        }

        public override string ToString()
        {
            return $"\"{Msg}\" Color: {Color}";
        }
    }
}
