﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task8
{
    public class FontStyle
    {
        public string fName { get; set; }
        public string fStyle { get; set; }
        public int fSize { get; set; }
        public FontStyle()
        { }
        public FontStyle(string name, string style, int size)
        {
            fStyle = style;
            fName = name;
            fSize = size;
        }
        public FontStyle(FontStyle prototype)
        {
            fStyle = prototype.fStyle;
            fName = prototype.fName;
            fSize = prototype.fSize;
        }

        public override string ToString()
        {
            return $"FontStyle: {fName} {fStyle} {fSize}pt ";
        }
    }
}
