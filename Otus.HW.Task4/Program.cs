﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Timers;

using System.Threading;
using System.Threading.Tasks;


namespace Otus.HW.Task4
{
    class Program
    {
        private static string targetDirectory = @"D:\Otus Labs";
        private static string fileName = "Паспорт.jpg";
        static CancellationTokenSource CToken = new CancellationTokenSource();

        private static List<string> FileNames = new List<string>();

        static void Main(string[] args)
        {

            FileDirectoryViewer DV = new FileDirectoryViewer();
            DV.FileFound += DV_FileFound;

            Console.WriteLine($"Поиск по файлам в папке {targetDirectory}\nДля завершения просмотра содержимого папки нажмите ESC\n");

            Task.Run(() =>
            {
                Console.WriteLine("> Поиск файлов запущен");

                DV.Start(targetDirectory, CToken.Token);

                var max = FileNames.GetMax(s => s == null ? 0 : s.Length);
                Console.WriteLine("");
                Console.WriteLine("> Поиск файлов завершён");
                if (max != null) Console.WriteLine("Максимальное имя файла в папке: " + max);

            }, CToken.Token);

            while (Console.ReadKey().Key != ConsoleKey.Escape)
            {
            }
            CToken.Cancel();

            DV.FileFound -= DV_FileFound;

            Console.ReadLine();
        }

        private static void DV_FileFound(object sender, FileArgs e)
        {
            FileNames.Add(e.FileName);

            Console.WriteLine($"{e.FileName}");

            //if (e.FileName == fileName) CToken.Cancel();
        }
    }
}
