﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW.Task3
{
    class MainServices
    {
        public static void StartWork()
        {
            #region Creating DataBase
            DBServices.CreateDB();
            DBServices.CreateUsersTable();
            DBServices.CreateCoursesTable();
            DBServices.CreatePaymentsTable();
            #endregion

            if (true)
            {
                #region Insert example data in DataBase
                List<User> Users = new List<User> {
                new User{
                Login = "IPetrov",
                Password = "pass_IPetrov",
                FirstName = "Иван",
                MiddleName = "Иванович",
                LastName = "Петров",
                Email = "PetrovI@mail.ru"
                },
                new User{
                Login = "AIvanov",
                Password = "pass_AIvanov",
                FirstName = "Андрей",
                MiddleName = "Петрович",
                LastName = "Иванов",
                Email = "IvanovA@mail.ru"
                },new User{
                Login = "NStrelnikov",
                Password = "pass_NStrelnikov",
                FirstName = "Николай",
                MiddleName = "Иванович",
                LastName = "Стрельников",
                Email = "StrelnikovN@mail.ru"
                },
                new User{
                Login = "ASergeeva",
                Password = "pass_ASergeeva",
                FirstName = "Антонина",
                MiddleName = "Петровна",
                LastName = "Сергеева",
                Email = "ISergeevaA@mail.ru"
                },
                new User{
                Login = "SNikitin",
                Password = "pass_SNikitin",
                FirstName = "Сергей",
                MiddleName = "Адреевич",
                LastName = "Никитин",
                Email = "NikitinS@mail.ru"
                }
            };

                List<Course> Courses = new List<Course> {
                new Course{
                Name = "Архитектура и шаблоны проектирования",
                Cost = 45000,
                //StartDate = new DateTime(2021,10,4),
                Duration = 4
                },
                new Course{
                Name = "С# Developer. Basic",
                Cost = 60000,
                //StartDate = new DateTime(2021,9,30),
                Duration = 5
                },
                new Course{
                Name = "С# Developer. Professional",
                Cost = 65000,
                //StartDate = new DateTime(2021,10,14),
                Duration = 6
                },
                new Course{
                Name = "С# ASP.NET Core разработчик",
                Cost = 90000,
                //StartDate = new DateTime(2021,8,25),
                Duration = 5
                },
                new Course{
                Name = "NoSQL",
                Cost = 50000,
                //StartDate = new DateTime(2021,9,29),
                Duration = 5.5
                }
            };

                foreach (User usr in Users)
                { DBServices.InsertUserInDB(usr); }

                foreach (Course course in Courses)
                { DBServices.InsertCourseInDB(course); }

                List<PaymentInfo> Payments = new List<PaymentInfo> {
                new PaymentInfo{
                    UserId = Users[0].Id,
                    CourseId = Courses[0].Id,
                    PaymentSum = Courses[0].Cost
                },
                new PaymentInfo{
                    UserId = Users[1].Id,
                    CourseId = Courses[1].Id,
                    PaymentSum = Courses[1].Cost
                },
                new PaymentInfo{
                    UserId = Users[2].Id,
                    CourseId = Courses[2].Id,
                    PaymentSum = Courses[2].Cost
                },
                new PaymentInfo{
                    UserId = Users[3].Id,
                    CourseId = Courses[3].Id,
                    PaymentSum = Courses[3].Cost
                },
                new PaymentInfo{
                    UserId = Users[4].Id,
                    CourseId = Courses[4].Id,
                    PaymentSum = Courses[4].Cost
                }
            };

                foreach (PaymentInfo payment in Payments)
                { DBServices.InsertPaymentInDB(payment); }
                #endregion
            }


            Console.WriteLine("==============================================");
            Console.WriteLine("Для работы с БД используйте следующие команды:");
            Console.WriteLine("  adduser - добавить нового пользователя");
            Console.WriteLine("  addcourse - добавить новый курс");
            Console.WriteLine("  addpayment - добавить новый платёж");
            Console.WriteLine("  getusers - получить список пользователей");
            Console.WriteLine("  getcourses - получить список курсов");
            Console.WriteLine("  getpayments - получить список платёжей");
            Console.WriteLine("==============================================");
            Console.WriteLine();
            string result;
            do
            {
                Console.WriteLine();
                Console.WriteLine("Введите команду:");
                result = Console.ReadLine();

                switch (result)
                {
                    case "adduser":
                        AddNewUser();
                        break;
                    case "addcourse":
                        AddNewCourse();
                        break;
                    case "addpayment":
                        AddNewPayment();
                        break;
                    case "getusers":
                        GetAllUsers();
                        break;
                    case "getcourses":
                        GetAllCourses();
                        break;
                    case "getpayments":
                        GetAllPayments();
                        break;
                }
            }
            while (result != "");

            Console.WriteLine();
        }

        public static void AddNewUser()
        {
            Console.WriteLine("------------------------------");
            Console.WriteLine("Добавление нового пользователя");
            Console.WriteLine("------------------------------");

            User NewUser = new User();

            Console.WriteLine("Логин:");
            NewUser.Login = Console.ReadLine();
            Console.WriteLine("Пароль:");
            NewUser.Password = Console.ReadLine();
            Console.WriteLine("Фамилия:");
            NewUser.LastName = Console.ReadLine();
            Console.WriteLine("Имя:");
            NewUser.FirstName = Console.ReadLine();
            Console.WriteLine("Отчество:");
            NewUser.MiddleName = Console.ReadLine();
            Console.WriteLine("E-mail:");
            NewUser.Email = Console.ReadLine();

            DBServices.InsertUserInDB(NewUser);

            Console.WriteLine("------------------------------");
        }

        public static void AddNewCourse()
        {
            Console.WriteLine("-----------------------");
            Console.WriteLine("Добавление нового курса");
            Console.WriteLine("-----------------------");

            Course NewCourse = new Course();

            Console.WriteLine("Наименование:");
            NewCourse.Name = Console.ReadLine();
            Console.WriteLine("Стоимость:");
            NewCourse.Cost = double.Parse(Console.ReadLine());
            Console.WriteLine("Длительность обучения в месяцах:");
            NewCourse.Duration = double.Parse(Console.ReadLine());

            DBServices.InsertCourseInDB(NewCourse);

            Console.WriteLine("------------------------------");
        }

        public static void AddNewPayment()
        {
            Console.WriteLine("-------------------------");
            Console.WriteLine("Добавление нового платежа");
            Console.WriteLine("-------------------------");

            PaymentInfo NewPayment = new PaymentInfo();

            Console.WriteLine("Студент (ID):");
            NewPayment.UserId = long.Parse(Console.ReadLine());
            Console.WriteLine("Курс (ID):");
            NewPayment.CourseId = long.Parse(Console.ReadLine());
            Console.WriteLine("Сумма платежа:");
            NewPayment.PaymentSum = double.Parse(Console.ReadLine());

            DBServices.InsertPaymentInDB(NewPayment);

            Console.WriteLine("------------------------------");
        }

        public static void GetAllUsers()
        {
            List<User> AllUsers = DBServices.GetUsersFromDB();

            if (AllUsers == null || AllUsers.Count == 0) Console.WriteLine("Users not found!");
            else
            {
                foreach (User Usr in AllUsers)
                {
                    Console.WriteLine(Usr.ToString());
                }
            }
        }

        public static void GetAllCourses()
        {
            List<Course> AllCourses = DBServices.GetCoursesFromDB();

            if (AllCourses == null || AllCourses.Count == 0) Console.WriteLine("Courses not found!");
            else
            {
                foreach (Course course in AllCourses)
                {
                    Console.WriteLine(course.ToString());
                }
            }
        }

        public static void GetAllPayments()
        {
            List<PaymentInfo> AllPayments = DBServices.GetPaymentsFromDB();

            if (AllPayments == null || AllPayments.Count == 0) Console.WriteLine("Payments not found!");
            else
            {
                foreach (PaymentInfo pay in AllPayments)
                {
                    Console.WriteLine(pay.ToString());
                }
            }
        }
    }
}
